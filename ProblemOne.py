class Sentences:

    snt_str = ""

    def __init__(self, snt_str):
        self.snt_str = snt_str

    def get_all_words(self):
        self.list_sentence = list()
        self.list_sentence = self.snt_str.split()
        return self.list_sentence

    def get_word(self, index):
        self.index = int(index)
        return self.list_sentence[self.index]

    def set_words(self, index, new_word):
        self.new_word = new_word
        self.index = int(index)
        self.list_sentence[self.index] = self.new_word

    def __str__(self):
        self.new_sent = self.list_sentence
        return self.listToString(self.new_sent)

    def listToString(self, newListWord):
        self.finalWord = newListWord

        str1 = " "

        return str1.join(self.finalWord)


if __name__ == "__main__":

    initialWord = "HELLO WORLD BLACK"
    indexTest = 0

    print("Word selected is: " + initialWord)
    words = Sentences(initialWord)

    print("Words in the sentence")
    print(words.get_all_words())

    print(
        "Returns only the word at a particular index in the sentence, your index is: "
        + str(indexTest)
    )
    print("Your word is: " + words.get_word(indexTest))

    print("Changes the word at a given index in sentence to a new word")

    print("Submit index")
    indexForReplace = input()

    print("Submit word")
    newWord = input()

    print(words.set_words(indexForReplace, newWord))

    print("Returns the sentence as a string using the built‐in method")
    print("Your previous sentence was: " + initialWord)
    print("Your NEW sentence was: " + words.__str__())
