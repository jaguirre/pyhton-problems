class Organ:
    pass

    OZ = 0.035274

    def __init__(
        self,
        organ_name,
        organ_weight_grams,
        is_vital_organ,
        organ_system,
        is_transplantable,
        organ_gender,
    ):
        self.organ_name = organ_name
        self.organ_weight_grams = organ_weight_grams
        self.is_vital_organ = is_vital_organ
        self.organ_system = organ_system
        self.is_transplantable = is_transplantable
        self.organ_gender = organ_gender

        def __str__(self):
            return (
                self.organ_name,
                self.organ_weight_grams,
                self.is_vital_organ,
                self.organ_system,
                self.is_transplantable,
                self.organ_gender,
            )


class Heart(Organ):
    pass

    def __init__(
        self,
        organ_name,
        is_vital_organ,
        organ_system,
        is_transplantable,
        organ_gender,
        heart_length_cm,
        organ_weight_grams,
        heart_thickness_cm,
        heart_breadth_cm,
    ):
        self.organ_name = (organ_name,)
        self.is_vital_organ = (is_vital_organ,)
        self.organ_system = (organ_system,)
        self.is_transplantable = (is_transplantable,)
        self.organ_gender = (organ_gender,)
        self.heart_length_cm = (heart_length_cm,)
        self.heart_weight_grams = (organ_weight_grams,)
        self.heart_thickness_cm = (heart_thickness_cm,)
        self.heart_breadth_cm = heart_breadth_cm

    def __str__(self):
        return (
            self.organ_name,
            self.is_vital_organ,
            self.organ_system,
            self.is_transplantable,
            self.organ_gender,
            self.heart_length_cm,
            self.heart_weight_grams,
            self.heart_thickness_cm,
            self.heart_breadth_cm,
        )

    def heart_status(self):
        return print("Pumping blood")

    def heart_weight_oz(self, s):
        return s * self.OZ


class Brain(Organ):
    pass

    def __init__(
        self,
        organ_name,
        is_vital_organ,
        organ_system,
        is_transplantable,
        organ_gender,
        brain_volume,
        organ_weight_grams,
    ):

        self.organ_name = (organ_name,)
        self.is_vital_organ = (is_vital_organ,)
        self.organ_system = (organ_system,)
        self.is_transplantable = (is_transplantable,)
        self.organ_gender = (organ_gender,)
        self.brain_volume = brain_volume
        self.brain_weight_gram = organ_weight_grams

    def __str__(self):
        return (
            self.organ_name,
            self.is_vital_organ,
            self.organ_system,
            self.is_transplantable,
            self.organ_gender,
            self.brain_volume,
            self.brain_weight_gram,
        )

    def brain_status(self):
        return print("Thinking")

    def brain_weight_oz(self, s):
        return str(s * self.OZ)


if __name__ == "__main__":

    weightInGramsBrain = 1200
    weightInGramsHeart = 600

    print("#################### Instantiating Object HEART #######################")

    instanceHeart = Heart(
        "Heart", "Yes", "Muscular system", "Yes", "Male", 12, 280, 6.0, 9.0
    )

    print("#################### Instantiating Object BRAIN #######################")
    instanceBrain = Brain("Brain", "Yes", "Nervous system", "No", "Male", 1260, 1370.0)

    print("#################### Object HEART #######################")
    print(instanceHeart.__str__())

    print("#################### Object BRAIN #######################")
    print(instanceBrain.__str__())

    print("###########################################")

    print(
        "Returns brain weight in ounces: "
        + "This your weight in grams: "
        + str(weightInGramsBrain)
        + " your brain is in oz: "
        + str(instanceBrain.brain_weight_oz(weightInGramsBrain))
    )

    print("###########################################")

    print(
        "Returns heart weight in ounces: "
        + "This your weight in grams: "
        + str(weightInGramsHeart)
        + " your heart is in oz: "
        + str(instanceHeart.heart_weight_oz(weightInGramsHeart))
    )

    print("#################### STATUS OF HEART #######################")
    instanceHeart.heart_status()

    print("#################### STATUS OF BRAIN #######################")
    instanceBrain.brain_status()
